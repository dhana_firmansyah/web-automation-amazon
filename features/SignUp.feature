Feature: Automate Sign Up form
  Background:
    Given User navigates to sign up page

  Scenario: Check Validation of form in fields
    When User tries to submit for with all empty fields
    Then User should see validations in name field

    Scenario Outline: Set invalid format to field
      When User set text to <field> fields
      Then User should see validations in respective field

      Examples:
      | field |
      | email |
      | pass  |

  Scenario: Check password and password confirmation not match
    When User enters password with invalid format
    Then User should see validations between password and confirm password fields

  Scenario Outline: User sign up with valid credentials
    When User enters name as <name> email as <email> password as <password> c_pass as <c_password>
    Then User should be redirected to captcha page

    Examples:
    |name|email|password|c_password|
    |Dhana|domdooom10@gmail.com|Mekari99|Mekari99|
