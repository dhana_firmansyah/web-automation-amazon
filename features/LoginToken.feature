Feature: Sign In Automation Scenario

Background:
Given User navigates to sign in page

Scenario: User Submit form with empty value
When User clicks continue in form
Then User should see validations to fill email

Scenario Outline: User Submit with unregistered email
When User input email with invalid format as <email>
Then User should see validations that email not found

Examples:
    |        email         |
    |      domdooom10      |

Scenario Outline: Usual login with valid email
When User input email as <email>
Then User is redirected to fill password as <password>
And User is redirected to captcha page

Examples:
  |        email         |      password        |
  |domdooom10@gmail.com  |     Jatinegara54     |

Scenario Outline:User forgot password
When User input email as <email>
Then User input email in forgot password page as <email>

Examples:
    |        email         |
    |domdooom10@gmail.com  |