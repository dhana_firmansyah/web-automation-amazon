require 'selenium-webdriver'
require "nokogiri"
driver = Selenium::WebDriver.for :chrome
driver.get "https://www.amazon.com/ap/register?openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fwww.amazon.com%2F%3F_encoding%3DUTF8%26ref_%3Dnav_newcust&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.assoc_handle=usflex&openid.mode=checkid_setup&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&"
wait = Selenium::WebDriver::Wait.new(:timeout => 15)

Given(/^User navigates to sign up page$/) do
  driver.navigate.to "https://www.amazon.com/ap/register?openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fwww.amazon.com%2F%3F_encoding%3DUTF8%26ref_%3Dnav_newcust&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.assoc_handle=usflex&openid.mode=checkid_setup&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&"
end

When(/^User set text to (.*) fields$/) do |field|
  driver.find_element(:id,'ap_email').click
  driver.find_element(:id,'ap_email').clear
  driver.find_element(:id,'ap_email').send_keys 'Selenium'
  driver.find_element(:id,'ap_password').click
  driver.find_element(:id,'ap_password').clear
  driver.find_element(:id,'ap_password').send_keys 'jtng'
  driver.find_element(:id,'continue').click
end

When(/^User tries to submit for with all empty fields$/) do
  puts "Element exists" if wait.until{
    /Create your Amazon account/.match(driver.page_source)
  }
  driver.find_element(:id,'continue').click
end

When(/^User enters password with invalid format$/) do
  driver.find_element(:id,'ap_password').click
  driver.find_element(:id,'ap_password').send_keys "asdfasdf"
  driver.find_element(:id,'ap_password_check').click
  driver.find_element(:id,'ap_password_check').send_keys "jatinegara"
  driver.find_element(:id,'continue').click
end

Then(/^User should see validations in name field$/) do
  puts "validation field name exists" if wait.until{
    /Enter your name/.match(driver.page_source)
  }
  puts "validation field email exists" if wait.until{
    /Enter your email/.match(driver.page_source)
  }
  puts "validation field password exists" if wait.until{
    /Enter your password/.match(driver.page_source)
  }
end

Then(/^User should see validations in respective field$/) do
  puts "validation email format exists" if wait.until{
    /Enter a valid email address/.match(driver.page_source)
  }
  puts "validation password < 6 exists" if wait.until{
    /Passwords must be at least 6 characters/.match(driver.page_source)
  }
  puts "validation confirm password exists" if wait.until{
    /Type your password again/.match(driver.page_source)
  }
end

Then(/^User should see validations between password and confirm password fields$/) do
  puts "validation password not match exist" if wait.until{
    /Passwords must match/.match(driver.page_source)
  }
end


When(/^User enters name as (.*) email as (.*) password as (.*) c_pass as (.*)$/) do |name, email, password, c_password|
  driver.find_element(:id,'ap_customer_name').click
  driver.find_element(:id,'ap_customer_name').send_keys name
  driver.find_element(:id,'ap_email').click
  driver.find_element(:id,'ap_email').send_keys email
  driver.find_element(:id,'ap_password').click
  driver.find_element(:id,'ap_password').send_keys password
  driver.find_element(:id,'ap_password_check').click
  driver.find_element(:id,'ap_password_check').send_keys c_password
  driver.find_element(:id,'continue').click
end

Then(/^User should be redirected to captcha page$/) do
  driver.quit
      #captcha cannot be automated
end