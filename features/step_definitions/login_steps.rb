require 'selenium-webdriver'
require "nokogiri"
driver = Selenium::WebDriver.for :chrome
driver.get "https://www.amazon.com/ap/signin?_encoding=UTF8&ignoreAuthState=1&openid.assoc_handle=usflex&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.mode=checkid_setup&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.ns.pape=http%3A%2F%2Fspecs.openid.net%2Fextensions%2Fpape%2F1.0&openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fwww.amazon.com%2F%3Fref_%3Dnav_custrec_signin&switch_account="
wait = Selenium::WebDriver::Wait.new(:timeout => 15)
Given(/^User navigates to sign in page$/) do
    driver.navigate.to"https://www.amazon.com/ap/signin?_encoding=UTF8&ignoreAuthState=1&openid.assoc_handle=usflex&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.mode=checkid_setup&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.ns.pape=http%3A%2F%2Fspecs.openid.net%2Fextensions%2Fpape%2F1.0&openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fwww.amazon.com%2F%3Fref_%3Dnav_custrec_signin&switch_account="
end

When(/^User clicks continue in form$/) do
  #   driver.find_element(:id,'continue-announce').click
  driver.find_element(:xpath,'//*[@id="continue"]//*[@class="a-button-input"]')
  driver.find_element(:xpath,'//*[@id="continue"]//*[@class="a-button-input"]').click
end

Then(/^User should see validations to fill email$/) do
    puts "Validation empty field email exists" if wait.until{
        /Enter your email or mobile phone number/.match(driver.page_source)
    }
end

When(/^User input email as (.*)$/) do |email|
    driver.find_element(:id,'ap_email').click
    driver.find_element(:id,'ap_email').clear
    driver.find_element(:id,'ap_email').send_keys email
    driver.find_element(:id,'continue').click
end

Then(/^User is redirected to fill password as (.*)$/) do |password|
    driver.find_element(:id,'ap_password').click
    driver.find_element(:id,'ap_password').clear
    driver.find_element(:id,'ap_password').send_keys password
    driver.find_element(:id,'signInSubmit').click
end

When(/^User input email with invalid format as (.*)$/) do |email|
    driver.find_element(:id,'ap_email').click
    driver.find_element(:id,'ap_email').clear
    driver.find_element(:id,'ap_email').send_keys email
    driver.find_element(:id,'continue').click
end

Then(/^User should see validations that email not found$/) do
    puts "Validation empty field email exists" if wait.until{
        /We cannot find an account with that email address/.match(driver.page_source)
    }
end

And(/^User is redirected to captcha page$/) do
    driver.find_element(:id,'continue').click
end

Then(/^User input email in forgot password page as (.*)$/) do |email|
    driver.find_element(:id,'auth-fpp-link-bottom').click
    driver.find_element(:id,'ap_email').click
    driver.find_element(:id,'ap_email').clear
    driver.find_element(:id,'ap_email').send_keys email
    driver.find_element(:id,'continue').click
end